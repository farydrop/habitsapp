package com.example.funnypuny.model

import java.time.DayOfWeek
import java.time.MonthDay

data class Calendar(
    val dayOfWeek: String,
    val day: String
)

val calendarData: ArrayList<Calendar> = arrayListOf(
    Calendar(
        "M",
        "26"
    ),
    Calendar(
        "T",
        "27"
    ),
    Calendar(
        "W",
        "28"
    ),
    Calendar(
        "T",
        "29"
    ),
    Calendar(
        "F",
        "30"
    ),
    Calendar(
        "S",
        "31"
    ),
    Calendar(
        "S",
        "1"
    ),
    Calendar(
        "m",
        "2"
    ),
    Calendar(
        "t",
        "3"
    ),
    Calendar(
        "w",
        "4"
    ),
    Calendar(
        "t",
        "5"
    ),
    Calendar(
        "f",
        "6"
    ),
    Calendar(
        "S",
        "7"
    ),
    Calendar(
        "S",
        "8"
    )


)

