package com.example.funnypuny

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.funnypuny.model.Calendar
import kotlinx.android.synthetic.main.calendar_item.view.*

class StartAdapter: RecyclerView.Adapter<StartAdapter.StartViewHolder>() {

    private var calendarList: MutableList<Calendar> = mutableListOf<Calendar>()

    class StartViewHolder(private val view: View):RecyclerView.ViewHolder(view){
        fun bind(calendar: Calendar){
            view.findViewById<TextView>(R.id.week).text = calendar.dayOfWeek
            view.findViewById<TextView>(R.id.number).text = calendar.day
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StartViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.calendar_item,parent,false)
        return StartViewHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: StartViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    private fun getItem(position: Int): Calendar{
        return calendarList[position]
    }

    fun addAll(generlist: List<Calendar>){
        this.calendarList.clear()
        this.calendarList.addAll(generlist)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return calendarList.size
    }

}

